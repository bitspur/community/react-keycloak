# react-keycloak

This is a continuation of the react-keycloak project

- https://github.com/react-keycloak/react-keycloak

---

![React Keycloak](/art/react-keycloak-logo.png?raw=true 'React Keycloak Logo')

# React Keycloak <!-- omit in toc -->

> React bindings for [Keycloak](https://www.keycloak.org/)

---

## Table of Contents <!-- omit in toc -->

- [Integrations](#integrations)
  - [React](#react)
  - [SSR](#ssr)
  - [React Native](#react-native)
- [Support](#support)
- [Examples](#examples)
- [Alternatives](#alternatives)
- [Contributing](#contributing)
- [License](#license)

---

## Integrations

### React

React Keycloak for Web requires:

- React **16.0** or later
- `keycloak-js` **9.0.2** or later

```shell
yarn add @bitspur/react-keycloak-web
```

or

```shell
npm install --save @bitspur/react-keycloak-web
```

or as a `UMD` package through `unpkg`

- one for development: https://unpkg.com/@bitspur/react-keycloak-web@latest/dist/umd/react-keycloak-web.js

- one for production: https://unpkg.com/@bitspur/react-keycloak-web@latest/dist/umd/react-keycloak-web.min.js

See `@bitspur/react-keycloak-web` package [README](https://github.com/react-keycloak/react-keycloak/blob/master/packages/web/README.md) for complete documentation.

### SSR

React Keycloak for SSR frameworks requires:

- React **16.0** or later
- SSR Framework:
  - NextJS **9** or later
  - Razzle **3** or later
- `keycloak-js` **9.0.2** or later

```shell
yarn add @bitspur/react-keycloak-ssr
```

or

```shell
npm install --save @bitspur/react-keycloak-ssr
```

See `@bitspur/react-keycloak-ssr` package [README](https://github.com/react-keycloak/react-keycloak/blob/master/packages/ssr/README.md) for complete documentation.

## Support

| version | keycloak-js version |
| ------- | ------------------- |
| v2.0.0+ | 9.0.2+              |
| v1.x    | >=8.0.2 <9.0.2      |

## Alternatives

If you need to connect using a more generic OIDC client instead of `keycloak.js`, consider using one of the following libraries:

- [bjerkio/oidc-react](https://github.com/bjerkio/oidc-react)
- [thchia/react-oidc](https://github.com/thchia/react-oidc)
- [@axa-fr/react-oidc](https://github.com/AxaGuilDEv/react-oidc)

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT
