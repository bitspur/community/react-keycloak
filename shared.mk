export CLOC ?= cloc
export CSPELL ?= $(call yarn_binary,cspell)
export ESLINT ?= $(call yarn_binary,eslint)
export JEST ?= $(call yarn_binary,jest)
export PRETTIER ?= $(call yarn_binary,prettier)
export TSC ?= $(call yarn_binary,tsc)

export CACHE_ENVS += \
        BABEL \
        BABEL_NODE \
        CLOC \
        CSPELL \
        ESLINT \
        JEST \
        PRETTIER \
        TSC
