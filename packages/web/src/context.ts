import type Keycloak from "@bitspur/keycloak-js";
import { createAuthContext } from "@bitspur/react-keycloak-core";

export const reactKeycloakWebContext = createAuthContext<Keycloak>();
export const ReactKeycloakWebContextConsumer = reactKeycloakWebContext.Consumer;
