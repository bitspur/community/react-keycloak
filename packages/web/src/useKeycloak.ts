import { reactKeycloakWebContext } from "./context";
import { useContext } from "react";

export function useKeycloak() {
  const ctx = useContext(reactKeycloakWebContext);
  if (!ctx?.authClient) return { initialized: false };
  const { authClient, initialized } = ctx;
  return {
    initialized,
    keycloak: authClient,
  };
}
