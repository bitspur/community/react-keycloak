import * as React from "react";
import type Keycloak from "@bitspur/keycloak-js";
import type { KeycloakConfig } from "@bitspur/keycloak-js";
import type { TokenPersistor } from "./persistors/types";
import { KeycloakProvider } from "./internals/KeycloakProvider";
import { getKeycloakInstance } from "./internals/keycloak";
import type {
  AuthClient,
  AuthClientInitOptions,
  AuthProviderProps,
  AuthClientEvent,
  AuthClientError,
  AuthClientTokens,
  AuthClientStub,
} from "@bitspur/react-keycloak-core";

export interface SSRKeycloakProviderProps<T extends AuthClientStub>
  extends Omit<AuthProviderProps<T>, "authClient"> {
  persistor: TokenPersistor;

  keycloakConfig: KeycloakConfig;
}

interface SSRKeycloakProviderState {
  initOptions: AuthClientInitOptions;

  keycloak: AuthClient;
}

export class SSRKeycloakProvider extends React.PureComponent<
  React.PropsWithChildren<SSRKeycloakProviderProps<AuthClientStub>>,
  SSRKeycloakProviderState
> {
  constructor(props: SSRKeycloakProviderProps<AuthClientStub>) {
    super(props);

    const { initOptions, keycloakConfig, persistor } = props;
    const cachedTokens = persistor.getTokens();

    this.state = {
      keycloak: getKeycloakInstance(keycloakConfig, persistor),
      initOptions: {
        ...cachedTokens,
        ...initOptions,
      },
    };
  }

  onEvent = (event: AuthClientEvent, error?: AuthClientError) => {
    if (event === "onReady") {
      if (!(this.state.keycloak as Keycloak).authenticated) {
        this.props?.persistor?.resetTokens();
      }
    }

    // Propagate events up
    this.props?.onEvent?.(event, error);
  };

  onTokens = (tokens: AuthClientTokens) => {
    // Persist updated tokens
    this.props?.persistor?.setTokens(tokens);

    // Propagate events up
    this.props?.onTokens?.(tokens);
  };

  render() {
    const { children, persistor, ...providerProps } = this.props;
    const { initOptions, keycloak } = this.state;

    return (
      <KeycloakProvider
        {...providerProps}
        authClient={keycloak}
        initOptions={initOptions}
        onEvent={this.onEvent}
        onTokens={this.onTokens}
      >
        {children}
      </KeycloakProvider>
    );
  }
}
