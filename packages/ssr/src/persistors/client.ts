import type { AuthClientTokens } from "@bitspur/react-keycloak-core";
import type { TokenPersistor } from "./types";
import { getCookie, removeCookie, setCookie } from "./utils";

export class Cookies implements TokenPersistor {
  setTokens({ idToken, token, refreshToken }: AuthClientTokens) {
    token && setCookie("kcToken", Buffer.from(token).toString("base64"));
    idToken && setCookie("kcIdToken", Buffer.from(idToken).toString("base64"));
    refreshToken &&
      setCookie("kcRefreshToken", Buffer.from(refreshToken).toString("base64"));
  }

  getTokens() {
    const idToken = getCookie("kcIdToken");
    const refreshToken = getCookie("kcRefreshToken");
    const token = getCookie("kcToken");
    return {
      idToken: idToken ? Buffer.from(idToken, "base64").toString() : undefined,
      token: token ? Buffer.from(token, "base64").toString() : undefined,
      refreshToken: refreshToken
        ? Buffer.from(refreshToken, "base64").toString()
        : undefined,
    };
  }

  resetTokens() {
    removeCookie("kcToken");
    removeCookie("kcIdToken");
    removeCookie("kcRefreshToken");
  }
}
