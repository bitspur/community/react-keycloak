import type { TokenPersistor } from "./types";
import { Buffer } from "buffer";
import { decode, encode, getCookie, removeCookie, setCookie } from "./utils";

export const ExpressCookies = (req: any): TokenPersistor => ({
  setTokens: () => {},
  getTokens: () => {
    const { kcIdToken, kcToken, kcRefreshToken } = req.cookies || {};
    return {
      token: kcToken ? Buffer.from(kcToken, "base64").toString() : undefined,
      idToken: kcIdToken
        ? Buffer.from(kcIdToken, "base64").toString()
        : undefined,
      refreshToken: kcRefreshToken
        ? Buffer.from(kcRefreshToken, "base64").toString()
        : undefined,
    };
  },
  resetTokens: () => {},
});

export const SSRCookies = (cookies: any): TokenPersistor => ({
  setTokens: ({ idToken, token, refreshToken }) => {
    !!idToken && setCookie("kcIdToken", encode(idToken));
    !!refreshToken && setCookie("kcRefreshToken", encode(refreshToken));
    !!token && setCookie("kcToken", encode(token));
  },
  getTokens: () => {
    const idToken = getCookie("kcIdToken", cookies);
    const refreshToken = getCookie("kcRefreshToken", cookies);
    const token = getCookie("kcToken", cookies);
    return {
      idToken: idToken ? decode(idToken) : "",
      refreshToken: refreshToken ? decode(refreshToken) : "",
      token: token ? decode(token) : "",
    };
  },
  resetTokens: () => {
    removeCookie("kcIdToken");
    removeCookie("kcRefreshToken");
    removeCookie("kcToken");
  },
});
