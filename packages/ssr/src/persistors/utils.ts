import Cookie from "js-cookie";
import { isServer } from "../internals/utils";

export function setCookie(name: string, val: string) {
  return Cookie.set(name, val);
}

export function getCookie(name: string, cookies: Record<string, string> = {}) {
  return isServer() ? cookies[name] : Cookie.get(name);
}

export function removeCookie(name: string) {
  Cookie.remove(name);
}

export function encode(value: string) {
  return Buffer.from(value).toString("base64");
}

export function decode(value: string) {
  return Buffer.from(value, "base64").toString();
}
