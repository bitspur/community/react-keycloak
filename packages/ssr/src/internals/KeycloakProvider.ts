import { createAuthProvider } from "@bitspur/react-keycloak-core";
import { reactKeycloakSsrContext } from "./context";

export const KeycloakProvider = createAuthProvider(reactKeycloakSsrContext);
