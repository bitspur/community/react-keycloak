import type { KeycloakConfig } from "@bitspur/keycloak-js";
import type { TokenPersistor } from "../persistors/types";
import { isServer } from "./utils";
import { isUserAuthenticated } from "@bitspur/react-keycloak-core";
import type {
  StubbedAuthClient,
  AuthClientStub,
} from "@bitspur/react-keycloak-core";

// Keycloak singleton
let keycloakInstance: StubbedAuthClient;

// This is a fake Keycloak instance we use to initialize Keycloak on the server
// and the client while the client is rehydrating.
// This gets over-written as soon as Keycloak is initialized on the client.
export function getKeycloakStub(persistor: TokenPersistor) {
  const kcTokens = persistor.getTokens();
  return {
    init: () => Promise.resolve(true),
    updateToken: () => Promise.resolve(false),
    idToken: kcTokens.idToken,
    token: kcTokens.token,
    refreshToken: kcTokens.refreshToken,
    authenticated: !!(kcTokens.idToken && kcTokens.token),
    _stub: true,
  } as unknown as AuthClientStub;
}

export function getKeycloakInstance(
  keycloakConfig: KeycloakConfig,
  persistor?: TokenPersistor,
  recreate = false
) {
  const isServerCheck = isServer();
  const keycloakStub = getKeycloakStub(persistor!);
  if (isServerCheck) return keycloakStub;
  const Keycloak = require("@bitspur/keycloak-js").default;
  if (recreate || !keycloakInstance) {
    keycloakInstance = new Keycloak(keycloakConfig);
    if (isUserAuthenticated(keycloakStub)) {
      keycloakInstance.stub = keycloakStub;
    }
  }
  return keycloakInstance;
}
