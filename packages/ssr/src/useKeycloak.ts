import type { AuthClientStub } from "@bitspur/react-keycloak-core";
import { isServer } from "./internals/utils";
import { reactKeycloakSsrContext } from "./internals/context";
import { useContext } from "react";

export type useKeycloakHookResults<T extends AuthClientStub> = {
  initialized: boolean;
  keycloak?: T;
};

export function useKeycloak<
  T extends AuthClientStub = AuthClientStub
>(): useKeycloakHookResults<T> {
  const { initialized, authClient } = useContext(reactKeycloakSsrContext);
  const initializedVar = initialized || isServer();
  return {
    initialized: initializedVar,
    keycloak: authClient as T | undefined,
  };
}
